# uoft-enrolment-charts

Generates charts of course enrolment numbers from data scraped by
[uoft-timetable-archive](https://github.com/freeatnet/uoft-timetable-archive)
(from the [University of Toronto Faculty of Arts and Science timetable](https://timetable.iit.artsci.utoronto.ca/)).


## Requirements

- [Python](https://www.python.org/)
- [Gnuplot](http://gnuplot.info/)
